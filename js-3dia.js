//Criar uma função que dado n arrays, retorne um novo array que possua apenas os valores que existem em todos os n arrays
function verifica(...arr){
    var novoArr = [];
    for(var i=0; i<arr[0].length; i++){
        for(var j=1; j<arr.length; j++){
            if(!(arr[j].includes(arr[0][i]))){
                break;
            }
            if(!(novoArr.includes(arr[0][i]))){
                novoArr.push(arr[0][i]);   
            }
        }
    }
    return novoArr;
}

let res = verifica( [120, 120, 110, 2] ,[110, 2, 130] );
console.log(res);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Crie uma função que dado n arrays, retorne apenas os que tenham a soma de seus elementos par

function par(...arr){
    var novoArr = [];
    for(let i=0; i<arr.length; i++){
        let res = arr[i].reduce( (total, valor_atual) => total + valor_atual );
        if(res%2==0){
            novoArr.push(arr[i]);
        }
    }
    console.log(novoArr);
}
par([1, 1, 3],[1, 2, 2, 2, 3] ,[2]);